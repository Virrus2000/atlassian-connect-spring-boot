package com.atlassian.connect.spring.internal.request.oauth2;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostUser;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Optional;

import static org.hamcrest.text.IsEqualIgnoringCase.equalToIgnoringCase;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OAuth2RestTemplateFactoryTest {

    @InjectMocks
    private OAuth2RestTemplateFactory authenticatedOAuthRestTemplateFactory;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private RestTemplateBuilder restTemplateBuilder;

    @Mock
    private RestTemplate restTemplate;

    @Before
    public void beforeEach() {
    }

    @Test
    public void throwsIfHostHasNoOauthClientId() {
        expectedException.expect(UnsupportedOperationException.class);
        AtlassianHost host = mock(AtlassianHost.class);
        AtlassianHostUser hostUser = new AtlassianHostUser(host, Optional.of("user"));
        authenticatedOAuthRestTemplateFactory.getOAuth2RestTemplate(hostUser);
    }

    @Test
    public void throwsIfHostUserHasNoUser() {
        expectedException.expect(IllegalArgumentException.class);
        AtlassianHost host = mock(AtlassianHost.class);
        when(host.getOauthClientId()).thenReturn("oauth-client-id");
        AtlassianHostUser hostUser = new AtlassianHostUser(host, Optional.empty());
        authenticatedOAuthRestTemplateFactory.getOAuth2RestTemplate(hostUser);
    }

    @Test
    public void usesDevAuthorizationServerForDevHosts() {
        AtlassianHost host = mock(AtlassianHost.class);
        when(host.getBaseUrl()).thenReturn("https://test.jira-dev.com/wiki");
        when(host.getOauthClientId()).thenReturn("oauth-client-id");
        OAuth2RestTemplate oauthRestTemplate = authenticatedOAuthRestTemplateFactory.getOAuth2RestTemplate(new AtlassianHostUser(host, Optional.of("user")));
        String accessTokenHost = URI.create(oauthRestTemplate.getResource().getAccessTokenUri()).getHost();
        assertThat(accessTokenHost, equalToIgnoringCase("auth.dev.atlassian.io"));
    }

    @Test
    public void usesProdAuthorizationServerForNonDevHosts() {
        AtlassianHost host = mock(AtlassianHost.class);
        when(host.getBaseUrl()).thenReturn("https://test.atlassian.net/wiki");
        when(host.getOauthClientId()).thenReturn("oauth-client-id");
        OAuth2RestTemplate oauthRestTemplate = authenticatedOAuthRestTemplateFactory.getOAuth2RestTemplate(new AtlassianHostUser(host, Optional.of("user")));
        String accessTokenHost = URI.create(oauthRestTemplate.getResource().getAccessTokenUri()).getHost();
        assertThat(accessTokenHost, equalToIgnoringCase("auth.atlassian.io"));
    }
}