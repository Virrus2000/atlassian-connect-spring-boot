package com.atlassian.connect.spring.internal.request.oauth2;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostUser;
import org.hamcrest.CustomTypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.hamcrest.MockitoHamcrest;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpMethod.POST;

@RunWith(MockitoJUnitRunner.class)
public class JwtBearerAccessTokenProviderTest {

    private static final URI AUTHORIZATION_SERVER_URL = URI.create("https://example.com");
    private Map<String, String> accessTokenResponse;

    private JwtBearerAccessTokenProvider jwtBearerAccessTokenProvider;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private OAuth2JwtAssertionGenerator jwtAssertionGenerator;

    @SuppressWarnings("unchecked")
    @Before
    public void beforeEach() {
        AtlassianHost host = mock(AtlassianHost.class);
        accessTokenResponse = new HashMap<>();
        accessTokenResponse.put("access_token", "access-token");
        accessTokenResponse.put("token_type", "urn:ietf:params:oauth:grant-type:jwt-bearer");
        accessTokenResponse.put("expires_in", "3600");

        jwtBearerAccessTokenProvider = new JwtBearerAccessTokenProvider(new AtlassianHostUser(host, Optional.of("user")),
                AUTHORIZATION_SERVER_URL, restTemplate, jwtAssertionGenerator);
        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(ParameterizedTypeReference.class)))
                .thenReturn(new ResponseEntity<>(accessTokenResponse, HttpStatus.OK));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void makesPostRequestToProvidedAuthorizationServerHost() {
        jwtBearerAccessTokenProvider.obtainAccessToken(new JwtBearerResourceDetails("clientKey", "secret", AUTHORIZATION_SERVER_URL.toString()), null);
        verify(restTemplate).exchange(eq(AUTHORIZATION_SERVER_URL.toASCIIString() + "/oauth2/token"), eq(POST),
                any(HttpEntity.class), any(ParameterizedTypeReference.class));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void makesRequestWithOnSpecParameters() {
        when(jwtAssertionGenerator.getAssertionString(any(), any())).thenReturn("the-assertion");
        jwtBearerAccessTokenProvider.obtainAccessToken(new JwtBearerResourceDetails("clientKey", "secret", AUTHORIZATION_SERVER_URL.toString()), null);
        verify(restTemplate).exchange(anyString(), any(HttpMethod.class), MockitoHamcrest.argThat(conformsToJwtBearerSpec()), any(ParameterizedTypeReference.class));
    }

    private static OnSpecRequestEntity conformsToJwtBearerSpec() {
        return new OnSpecRequestEntity("Request conforming to RFC-7521");
    }

    private static class OnSpecRequestEntity extends CustomTypeSafeMatcher<HttpEntity<MultiValueMap<String, String>>> {

        public OnSpecRequestEntity(String description) {
            super(description);
        }

        @Override
        protected boolean matchesSafely(HttpEntity<MultiValueMap<String, String>> request) {
            final MultiValueMap<String, String> body = request.getBody();
            return Objects.equals(body.getFirst("assertion"), "the-assertion") &&
                    Objects.equals(body.getFirst("grant_type"), "urn:ietf:params:oauth:grant-type:jwt-bearer");
        }
    }
}