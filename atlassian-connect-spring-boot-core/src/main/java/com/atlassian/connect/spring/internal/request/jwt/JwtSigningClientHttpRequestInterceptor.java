package com.atlassian.connect.spring.internal.request.jwt;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.internal.auth.AtlassianConnectSecurityContextHelper;
import com.atlassian.connect.spring.internal.request.AtlassianConnectHttpRequestInterceptor;
import com.atlassian.connect.spring.internal.request.AtlassianHostUriResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * A {@link ClientHttpRequestInterceptor} that signs requests to Atlassian hosts with JSON Web Tokens.
 */
@Component
public class JwtSigningClientHttpRequestInterceptor extends AtlassianConnectHttpRequestInterceptor {

    private JwtGenerator jwtGenerator;

    private AtlassianHostUriResolver hostUriResolver;

    private AtlassianConnectSecurityContextHelper securityContextHelper;

    @Autowired
    public JwtSigningClientHttpRequestInterceptor(JwtGenerator jwtGenerator,
            AtlassianHostUriResolver hostUriResolver,
            AtlassianConnectSecurityContextHelper securityContextHelper,
            @Value("${atlassian.connect.client-version}") String atlassianConnectClientVersion) {
        super(atlassianConnectClientVersion);
        this.jwtGenerator = jwtGenerator;
        this.hostUriResolver = hostUriResolver;
        this.securityContextHelper = securityContextHelper;
    }

    protected Optional<AtlassianHost> getHostForRequest(HttpRequest request) {
        Optional<AtlassianHost> optionalHost = securityContextHelper.getHostFromSecurityContext()
                .filter((host) -> AtlassianHostUriResolver.isRequestToHost(request.getURI(), host));
        if (!optionalHost.isPresent()) {
            optionalHost = hostUriResolver.getHostFromRequestUrl(request.getURI());
        }
        return optionalHost;
    }

    @Override
    protected HttpRequest rewrapRequest(HttpRequest request, AtlassianHost host) {
        String jwt = jwtGenerator.createJwtToken(request.getMethod(), request.getURI(), host);
        request.getHeaders().set(HttpHeaders.AUTHORIZATION, String.format("JWT %s", jwt));
        return request;
    }
}
