package com.atlassian.connect.spring.internal.jwt;

import com.nimbusds.jose.JWSObject;
import com.nimbusds.jwt.JWTClaimsSet;

import java.text.ParseException;

public class JwtParser {

    public JWTClaimsSet parse(String jwt) throws JwtParseException {
        JWSObject jwsObject = parseJWSObject(jwt);
        try {
            return JWTClaimsSet.parse(jwsObject.getPayload().toJSONObject());
        } catch (ParseException e) {
            throw new JwtParseException(e);
        }
    }

    private JWSObject parseJWSObject(String jwt) throws JwtParseException {
        JWSObject jwsObject;

        try {
            jwsObject = JWSObject.parse(jwt);
        } catch (ParseException e) {
            throw new JwtParseException(e);
        }
        return jwsObject;
    }
}
