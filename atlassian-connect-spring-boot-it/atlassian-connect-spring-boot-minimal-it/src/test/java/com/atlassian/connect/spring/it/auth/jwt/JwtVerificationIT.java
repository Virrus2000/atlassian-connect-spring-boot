package com.atlassian.connect.spring.it.auth.jwt;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.internal.jwt.CanonicalHttpRequest;
import com.atlassian.connect.spring.internal.request.jwt.JwtBuilder;
import com.atlassian.connect.spring.internal.request.jwt.JwtQueryHashGenerator;
import com.atlassian.connect.spring.internal.request.jwt.SelfAuthenticationTokenGenerator;
import com.atlassian.connect.spring.it.util.AtlassianHostJwtSigningClientHttpRequestInterceptor;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import com.atlassian.connect.spring.it.util.SimpleJwtSigningRestTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

import static com.atlassian.connect.spring.it.util.AtlassianHosts.createAndSaveHost;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class JwtVerificationIT extends BaseApplicationIT {

    @Autowired
    private AddonDescriptorLoader addonDescriptorLoader;

    @Test
    public void shouldReturnPrincipalDetailsForValidJwtHeader() throws Exception {
        AtlassianHost host = createAndSaveHost(hostRepository);
        String subject = "charlie";
        TestRestTemplate restTemplate = new SimpleJwtSigningRestTemplate(host, Optional.of(subject));
        ResponseEntity<AtlassianHostUser> response = restTemplate.getForEntity(getPrincipalRequestUri(), AtlassianHostUser.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getHost().getClientKey(), is(host.getClientKey()));
        assertThat(response.getBody().getUserKey(), is(Optional.of(subject)));
    }

    @Test
    public void shouldReturnPrincipalDetailsForValidJwtQueryParameter() throws Exception {
        AtlassianHost host = createAndSaveHost(hostRepository);
        AtlassianHostJwtSigningClientHttpRequestInterceptor interceptor = new AtlassianHostJwtSigningClientHttpRequestInterceptor(
                host.getClientKey(), host.getSharedSecret(), Optional.empty());
        UriComponentsBuilder requestUriBuilder = getPrincipalRequestUriBuilder();
        String jwt = interceptor.createJwt(HttpMethod.GET, requestUriBuilder.build().toUri());
        URI requestUri = requestUriBuilder.queryParam("jwt", jwt).build().toUri();
        ResponseEntity<AtlassianHostUser> response = new RestTemplate().getForEntity(requestUri, AtlassianHostUser.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getHost().getClientKey(), is(host.getClientKey()));
        assertThat(response.getBody().getUserKey(), is(Optional.empty()));
    }

    @Test
    public void shouldReturnPrincipalDetailsForValidSelfAuthenticationTokenHeader() {
        AtlassianHost host = createAndSaveHost(hostRepository);
        String subject = "charlie";
        String addonKey = getAddonKey();
        ResponseEntity<AtlassianHostUser> response = getWithJwt(
                createJwtBuilder(addonKey, host.getSharedSecret())
                        .audience(addonKey)
                        .subject(subject)
                        .claim(SelfAuthenticationTokenGenerator.HOST_CLIENT_KEY_CLAIM, host.getClientKey()).build());
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getHost().getClientKey(), is(host.getClientKey()));
        assertThat(response.getBody().getUserKey(), is(Optional.of(subject)));
    }

    @Test
    public void shouldReturnPrincipalDetailsForProvidedSelfAuthenticationTokenQueryParameter() {
        AtlassianHost host = createAndSaveHost(hostRepository);
        String subject = "charlie";
        TestRestTemplate restTemplate = new SimpleJwtSigningRestTemplate(host, Optional.of(subject));
        URI modelRequestUri = getRequestUriBuilder().path("/token").build().toUri();
        String selfAuthenticationToken = restTemplate.getForObject(modelRequestUri, String.class);
        ResponseEntity<AtlassianHostUser> response = getWithJwt(selfAuthenticationToken);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getHost().getClientKey(), is(host.getClientKey()));
        assertThat(response.getBody().getUserKey(), is(Optional.of(subject)));
    }

    @Test
    public void shouldRejectRequestWithoutJwt() throws Exception {
        TestRestTemplate restTemplate = new TestRestTemplate();
        ResponseEntity<Void> response = restTemplate.getForEntity(getPrincipalRequestUri(), Void.class);
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
        assertThat(response.getHeaders().getFirst(HttpHeaders.WWW_AUTHENTICATE), startsWith("JWT "));
    }

    @Test
    public void shouldRejectRequestWithMalformedJwt() throws Exception {
        ResponseEntity<AtlassianHostUser> response = getWithJwt("malformed-jwt");
        assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void shouldRejectRequestForUnknownHost() throws Exception {
        ResponseEntity<AtlassianHostUser> response = getWithJwt(createJwtBuilderWithQsh("unknown-host", "some-secret", getPrincipalRequestUri()).build());
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
    }

    @Test
    public void shouldRejectRequestWithExpiredJwt() throws Exception {
        AtlassianHost host = createAndSaveHost(hostRepository);
        long issuedAt = System.currentTimeMillis() / 1000 + -1001L;
        long expirationTime = issuedAt + 1L;
        ResponseEntity<AtlassianHostUser> response = getWithJwt(
                createJwtBuilderWithQsh(host.getClientKey(), host.getSharedSecret(), getPrincipalRequestUri())
                        .issuedAt(issuedAt)
                        .expirationTime(expirationTime).build());
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
    }

    @Test
    public void shouldRejectRequestWithInvalidSecret() throws Exception {
        AtlassianHost host = createAndSaveHost(hostRepository);
        ResponseEntity<AtlassianHostUser> response = getWithJwt(
                createJwtBuilderWithQsh(host.getClientKey(), "invalid-secret", getPrincipalRequestUri()).build());
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
    }

    @Test
    public void shouldAllowRequestWithoutQsh() throws Exception {
        AtlassianHost host = createAndSaveHost(hostRepository);
        ResponseEntity<AtlassianHostUser> response = getWithJwt(createJwtBuilder(
                host.getClientKey(), host.getSharedSecret()).build());
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
    }

    @Test
    public void shouldRejectRequestWithInvalidQsh() throws Exception {
        AtlassianHost host = createAndSaveHost(hostRepository);
        ResponseEntity<AtlassianHostUser> response = getWithJwt(createJwtBuilderWithQsh(
                host.getClientKey(), host.getSharedSecret(), URI.create(getServerAddress() + "/not/valid")).build());
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
    }

    @Test
    public void shouldRejectRequestWithSelfAuthenticationTokenWithoutAudience() {
        AtlassianHost host = createAndSaveHost(hostRepository);
        ResponseEntity<AtlassianHostUser> response = getWithJwt(
                createJwtBuilder(getAddonKey(), host.getSharedSecret())
                        .claim(SelfAuthenticationTokenGenerator.HOST_CLIENT_KEY_CLAIM, host.getClientKey()).build());
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
    }

    @Test
    public void shouldRejectRequestWithSelfAuthenticationTokenWithInvalidAudience() {
        AtlassianHost host = createAndSaveHost(hostRepository);
        ResponseEntity<AtlassianHostUser> response = getWithJwt(
                createJwtBuilder(getAddonKey(), host.getSharedSecret())
                        .audience("foo")
                        .claim(SelfAuthenticationTokenGenerator.HOST_CLIENT_KEY_CLAIM, host.getClientKey()).build());
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
    }

    @Test
    public void shouldRejectRequestWithSelfAuthenticationTokenWithoutClientKey() {
        String addonKey = getAddonKey();
        ResponseEntity<AtlassianHostUser> response = getWithJwt(
                createJwtBuilder(addonKey, "foo")
                        .audience(addonKey).build());
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
    }

    private ResponseEntity<AtlassianHostUser> getWithJwt(String jwt) {
        return new SimpleJwtSigningRestTemplate(jwt).getForEntity(getPrincipalRequestUri(), AtlassianHostUser.class);
    }

    private JwtBuilder createJwtBuilder(String clientKey, String sharedSecret) {
        return new JwtBuilder()
                .issuer(clientKey)
                .signature(sharedSecret);
    }

    private JwtBuilder createJwtBuilderWithQsh(String clientKey, String sharedSecret, URI requestUri) {
        JwtQueryHashGenerator queryHashGenerator = new JwtQueryHashGenerator();
        CanonicalHttpRequest canonicalHttpRequest = queryHashGenerator.createCanonicalHttpRequest(HttpMethod.GET, requestUri, getServerAddress());
        String queryHash = queryHashGenerator.computeCanonicalRequestHash(canonicalHttpRequest);
        return createJwtBuilder(clientKey, sharedSecret)
                .queryHash(queryHash);
    }

    private URI getPrincipalRequestUri() {
        return getPrincipalRequestUriBuilder().build().toUri();
    }

    private UriComponentsBuilder getPrincipalRequestUriBuilder() {
        return getRequestUriBuilder().path("/jwt");
    }

    private UriComponentsBuilder getRequestUriBuilder() {
        return UriComponentsBuilder.fromUri(URI.create(getServerAddress()));
    }

    private String getAddonKey() {
        return addonDescriptorLoader.getDescriptor().getKey();
    }

    @TestConfiguration
    public static class JwtVerificationConfiguration {

        @Bean
        public JwtPrincipalController jwtPrincipalController() {
            return new JwtPrincipalController();
        }

        @Bean
        public SelfAuthenticationTokenController selfAuthenticationTokenController() {
            return new SelfAuthenticationTokenController();
        }
    }

    @RestController
    public static class JwtPrincipalController {

        @GetMapping(value = "/jwt", produces = "application/json")
        public AtlassianHostUser getPrincipal(@AuthenticationPrincipal AtlassianHostUser hostUser) {
            return hostUser;
        }
    }

    @RestController
    public static class SelfAuthenticationTokenController {

        @GetMapping(value = "/token")
        public String getSelfAuthenticationToken(Model model) {
            return (String)model.asMap().get("atlassianConnectToken");
        }
    }
}
