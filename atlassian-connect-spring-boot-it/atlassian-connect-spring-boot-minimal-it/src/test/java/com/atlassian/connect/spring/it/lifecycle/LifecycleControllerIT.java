package com.atlassian.connect.spring.it.lifecycle;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static com.atlassian.connect.spring.it.util.AtlassianHosts.BASE_URL;
import static com.atlassian.connect.spring.it.util.AtlassianHosts.CLIENT_KEY;
import static com.atlassian.connect.spring.it.util.AtlassianHosts.SHARED_SECRET;
import static com.atlassian.connect.spring.it.util.LifecycleBodyHelper.createLifecycleJson;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LifecycleControllerIT extends BaseApplicationIT {

    @Test
    public void shouldStoreHostOnFirstInstall() throws Exception {
        mvc.perform(postInstalled("/installed", SHARED_SECRET))
                .andExpect(status().isNoContent());

        AtlassianHost installedHost = hostRepository.findById(CLIENT_KEY).get();
        assertThat(installedHost, notNullValue());
        assertThat(installedHost.getSharedSecret(), is(SHARED_SECRET));
        assertThat(installedHost.getBaseUrl(), is(BASE_URL));
        assertThat(installedHost.isAddonInstalled(), is(true));
    }

    @Test
    public void shouldUpdateSharedSecretOnSignedSecondInstall() throws Exception {
        AtlassianHost host = saveHost(CLIENT_KEY, SHARED_SECRET, BASE_URL);
        String newSharedSecret = "some-other-secret";
        setJwtAuthenticatedPrincipal(host);
        mvc.perform(postInstalled("/installed", newSharedSecret))
                .andExpect(status().isNoContent());

        AtlassianHost installedHost = hostRepository.findById(CLIENT_KEY).get();
        assertThat(installedHost, notNullValue());
        assertThat(installedHost.getSharedSecret(), is(newSharedSecret));
    }

    @Test
    public void shouldRejectSecondInstallWithoutJwt() throws Exception {
        saveHost(CLIENT_KEY, SHARED_SECRET, BASE_URL);
        mvc.perform(postInstalled("/installed", SHARED_SECRET))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void shouldRejectSharedSecretUpdateByOtherHost() throws Exception {
        saveHost(CLIENT_KEY, SHARED_SECRET, BASE_URL);
        AtlassianHost otherHost = saveHost("other-host", "other-secret", "http://other-example.com");
        setJwtAuthenticatedPrincipal(otherHost);
        mvc.perform(postInstalled("/installed", "some-other-secret"))
                .andExpect(status().isForbidden());    }

    @Test
    public void shouldRejectUninstallByOtherHost() throws Exception {
        saveHost(CLIENT_KEY, SHARED_SECRET, BASE_URL);
        AtlassianHost otherHost = saveHost("other-host", "other-secret", "http://other-example.com");
        setJwtAuthenticatedPrincipal(otherHost);
        mvc.perform(postUninstalled("/uninstalled", SHARED_SECRET))
                .andExpect(status().isForbidden());
    }

    @Test
    public void shouldSoftDeleteHostOnUninstall() throws Exception {
        AtlassianHost host = saveHost(CLIENT_KEY, SHARED_SECRET, BASE_URL);
        setJwtAuthenticatedPrincipal(host);
        mvc.perform(postUninstalled("/uninstalled", SHARED_SECRET))
                .andExpect(status().isNoContent());

        AtlassianHost installedHost = hostRepository.findById(CLIENT_KEY).get();
        assertThat(installedHost, notNullValue());
        assertThat(installedHost.getSharedSecret(), is(SHARED_SECRET));
        assertThat(installedHost.getBaseUrl(), is(BASE_URL));
        assertThat(installedHost.isAddonInstalled(), is(false));
    }

    @Test
    public void shouldIgnoreMissingHostOnUninstall() throws Exception {
        mvc.perform(postUninstalled("/uninstalled", SHARED_SECRET))
                .andExpect(status().isNoContent());

        AtlassianHost installedHost = hostRepository.findById(CLIENT_KEY).get();
        assertThat(installedHost, nullValue());
    }

    @Test
    public void shouldRejectInstallWithoutBody() throws Exception {
        mvc.perform(post("/installed")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldRejectInstallWithInvalidBody() throws Exception {
        mvc.perform(post("/installed")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldRejectInstallWithInvalidEventType() throws Exception {
        mvc.perform(post("/installed")
                .contentType(MediaType.APPLICATION_JSON)
                .content(createLifecycleJson("uninstalled", "some-secret")))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldRejectUninstallWithoutBody() throws Exception {
        mvc.perform(post("/uninstalled")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldRejectUninstallWithInvalidBody() throws Exception {
        mvc.perform(post("/uninstalled")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldRejectUninstallWithInvalidEventType() throws Exception {
        mvc.perform(post("/uninstalled")
                .contentType(MediaType.APPLICATION_JSON)
                .content(createLifecycleJson("installed", "some-secret")))
                .andExpect(status().isBadRequest());
    }

    public MockHttpServletRequestBuilder postInstalled(String path, String secret) throws Exception {
        return postLifecycleCallback(path, "installed", secret);
    }

    public MockHttpServletRequestBuilder postUninstalled(String path, String secret) throws Exception {
        return postLifecycleCallback(path, "uninstalled", secret);
    }

    private MockHttpServletRequestBuilder postLifecycleCallback(String path, String eventType, String secret) throws JsonProcessingException {
        return post(path)
                .contentType(MediaType.APPLICATION_JSON)
                .content(createLifecycleJson(eventType, secret));
    }

    private AtlassianHost saveHost(String clientKey, String sharedSecret, String baseUrl) {
        AtlassianHost host = new AtlassianHost();
        host.setClientKey(clientKey);
        host.setSharedSecret(sharedSecret);
        host.setBaseUrl(baseUrl);
        hostRepository.save(host);
        return host;
    }
}
