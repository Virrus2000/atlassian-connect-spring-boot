package com.atlassian.connect.spring.it.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.rule.OutputCapture;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
public class NoHostRepositoryIT {

    @Rule
    public OutputCapture capture = new OutputCapture();

    @Test
    public void shouldPrintFailureAnalysisWhenMissingAtlassianHostRepository() {
        try {
            new SpringApplication(NoDataAddonApplication.class).run();
        } catch (Exception e) {
            // We are inspecting the log instead
        }

        String log = capture.toString();
        assertThat(log, allOf(containsString("APPLICATION FAILED TO START"),
                containsString("Choose a Spring Data implementation to use with AtlassianHostRepository")));
    }
}
