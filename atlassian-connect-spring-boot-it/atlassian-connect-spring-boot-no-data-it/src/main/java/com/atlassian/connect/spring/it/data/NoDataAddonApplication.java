package com.atlassian.connect.spring.it.data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NoDataAddonApplication {

    public static void main(String[] args) {
        new SpringApplication(NoDataAddonApplication.class).run(args);
    }
}
